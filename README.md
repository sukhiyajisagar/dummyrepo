Features Supported by Framework :

Parallel execution with multiple browsers.
Integrated with Jenkins effortlessly.
Extent reporting which works in parallel runs. ( Location -  reports/index.html )
Log4j logging ( Location -  logs/prints.logs )

Other Details: 

An simple robust framework for selenium build with java. It will generate the extent report with the screenshots appended to the report.
Url, Username, Password, Browser can be given easily from data.properties file.

How to use this framework?

Clone the repository to your workspace.
Open the data.properties under the src/main/java/resources folder
In the browser -->Give "chrome"(By default given) for running TC in chrome browser
The browser will be passed to the base class.
Run the testng.xml file (if testNG plugin installed in your IDE). You can even run as mvn test which will trigger the testng.xml

Note:

src/test/java - contains testcases
src/main/java - contains page objects, resources ( like ltestNG istener, log4j configuration, extent report, chrome driver exe, property file, base class ) 