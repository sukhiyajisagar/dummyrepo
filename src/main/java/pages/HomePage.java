package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {

	WebDriver driver;

	public HomePage(WebDriver driver) {
		this.driver = driver;
	}

	private By profileIcon = By.id("dLabel");
	private By logoutLink = By.linkText("Log Out");
	private By addPortfolioButton = By.xpath("//button//span[text()='Add Portfolio']");

	public WebElement getProfileIcon() {
		return driver.findElement(profileIcon);
	}

	public WebElement getLogoutLink() {
		return driver.findElement(logoutLink);
	}
	
	public WebElement getAddPortfolioButton() {
		return driver.findElement(addPortfolioButton);
	}
}
