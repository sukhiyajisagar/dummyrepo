package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {

	WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}

	private By username = By.id("username");
	private By password = By.id("password");
	private By loginButton = By.xpath("//button[@class='btn btn-primary btn-block']");
	private By rememberMe = By.xpath("//span[@class='check']");
	private By usernameValidationMessage = By.cssSelector("#username+p");
	private By passwordValidationMessage = By.cssSelector("#password+p");
	private By invalidPasswordMessage = By.xpath("//form[@name='loginForm'] //p");

	public WebElement getUsername() {
		return driver.findElement(username);
	}

	public WebElement getPassword() {
		return driver.findElement(password);
	}

	public WebElement getLoginButton() {
		return driver.findElement(loginButton);
	}

	public WebElement getRememberMe() {
		return driver.findElement(rememberMe);
	}

	public WebElement getUsernameMessage() {
		return driver.findElement(usernameValidationMessage);
	}

	public WebElement getPasswordMessage() {
		return driver.findElement(passwordValidationMessage);
	}

	public WebElement getInvalidPasswordMessage() {
		return driver.findElement(invalidPasswordMessage);
	}
}
