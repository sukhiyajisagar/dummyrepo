package testcases;

import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.aventstack.extentreports.Status;
import pages.LoginPage;
import resources.Base;

public class AccessibilityWithKey extends Base {

	public WebDriver driver;
	static final Logger log = LogManager.getLogger(AccessibilityWithKey.class.getName());

	@BeforeTest
	public void initialize() throws IOException {
		log.debug("Opening chrome browser");
		driver = initializeDriver();
	}

	@Test
	public void accessibilityWithKey() throws InterruptedException {
		driver.get(prop.getProperty("url"));
		log.debug("SigFig website launched");
		extentTest.get().log(Status.INFO, "SigFig site launched");
		Actions actions = new Actions(driver);
		int counter = 0;
		while (counter < 6) {
			actions.sendKeys(Keys.TAB).build().perform();
			counter++;
		}
		actions.sendKeys(Keys.ENTER);
		log.debug("Login button of default page clicked with enter key");
		extentTest.get().log(Status.INFO, "User clicked enter key to login");
		LoginPage loginPageObj = new LoginPage(driver);
		actions.sendKeys(Keys.TAB).build().perform();
		log.debug("User pressed tab key");
		extentTest.get().log(Status.INFO, "User pressed tab key");
		actions.sendKeys(Keys.TAB).build().perform();
		log.debug("User pressed tab key");
		extentTest.get().log(Status.INFO, "User pressed tab key");
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOf(loginPageObj.getUsernameMessage()));
		String expectedMessage = "Please enter your username";
		String actualMessage = loginPageObj.getUsernameMessage().getText();
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(actualMessage, expectedMessage);
		WebDriverWait wait2 = new WebDriverWait(driver, 5);
		wait2.until(ExpectedConditions.visibilityOf(loginPageObj.getPasswordMessage()));
		String expectedMessage2 = "Please enter your password";
		String actualMessage2 = loginPageObj.getPasswordMessage().getText();
		softAssert.assertEquals(actualMessage2, expectedMessage2);
		softAssert.assertAll();
		String validationMessage = REPORTACTUALSTRING + actualMessage + REPORTEXPECTEDSTRING + expectedMessage + "]";
		extentTest.get().log(Status.INFO, validationMessage);
		String validationMessage2 = REPORTACTUALSTRING + actualMessage2 + REPORTEXPECTEDSTRING + expectedMessage2 + "]";
		extentTest.get().log(Status.INFO, validationMessage2);
		log.debug("Validation success");
	}

	@AfterTest
	public void teardown() {
		log.debug("Closing browser");
		driver.close();
	}

}
