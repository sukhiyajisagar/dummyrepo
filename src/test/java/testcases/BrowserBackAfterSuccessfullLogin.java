package testcases;

import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import pages.DefaultPage;
import pages.LoginPage;
import resources.Base;

public class BrowserBackAfterSuccessfullLogin extends Base {

	public WebDriver driver;
	static final Logger log = LogManager.getLogger(BrowserBackAfterSuccessfullLogin.class.getName());

	@BeforeTest
	public void initialize() throws IOException {
		log.debug("Opening chrome browser");
		driver = initializeDriver();
	}

	@Test
	public void browserBackAfterSuccessfullLogin() throws InterruptedException {
		driver.get(prop.getProperty("url"));
		log.debug("SigFig website launched");
		extentTest.get().log(Status.INFO, "SigFig site launched");
		DefaultPage defaultPageObj = new DefaultPage(driver);
		defaultPageObj.getLoginButtonWebelement().click();
		log.debug("Login button of default page clicked");
		extentTest.get().log(Status.INFO, "User clicked on login button of default page");
		LoginPage loginPageObj = new LoginPage(driver);
		loginPageObj.getUsername().sendKeys(prop.getProperty("username"));
		log.debug("Username inputted");
		extentTest.get().log(Status.INFO, "User input " + prop.getProperty("username") + " in username");
		loginPageObj.getPassword().sendKeys(prop.getProperty("password"));
		log.debug("Password inputted");
		extentTest.get().log(Status.INFO, "User input " + prop.getProperty("password") + " in password");
		loginPageObj.getLoginButton().click();
		log.debug("Login button clicked");
		extentTest.get().log(Status.INFO, "User clicked on login button to sign-in");
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions
				.elementToBeClickable(driver.findElement(By.xpath("//button//span[text()='Add Portfolio']"))));
		log.debug("User successfully logged in");
		extentTest.get().log(Status.INFO, "User successfully logged in");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("history.go(-1);", new Object[0]);
		log.debug("Browser back button clicked");
		extentTest.get().log(Status.INFO, "User clicked on browser back button");
		Assert.assertEquals(driver.getCurrentUrl(), "https://www.sigfig.com/f/#/managed");
		log.debug("User remains logged in");
		extentTest.get().log(Status.INFO, "User remains logged in");
	}

	@AfterTest
	public void teardown() {
		log.debug("Closing browser");
		driver.close();
	}
}
