package testcases;

import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import pages.DefaultPage;
import pages.HomePage;
import pages.LoginPage;
import resources.Base;

public class BrowserBackAfterSuccessfullLogout extends Base {

	public WebDriver driver;
	static final Logger log = LogManager.getLogger(BrowserBackAfterSuccessfullLogout.class.getName());

	@BeforeTest
	public void initialize() throws IOException {
		log.debug("Opening chrome browser");
		driver = initializeDriver();
	}

	@Test
	public void browserBackAfterSuccessfullLogout() throws InterruptedException {
		driver.get(prop.getProperty("url"));
		log.debug("SigFig website launched");
		extentTest.get().log(Status.INFO, "SigFig site launched");
		DefaultPage defaultPageObj = new DefaultPage(driver);
		defaultPageObj.getLoginButtonWebelement().click();
		log.debug("Login button of default page clicked");
		extentTest.get().log(Status.INFO, "User clicked on login button of default page");
		LoginPage loginPageObj = new LoginPage(driver);
		loginPageObj.getUsername().sendKeys(prop.getProperty("username"));
		log.debug("Username inputted");
		extentTest.get().log(Status.INFO, "User input " + prop.getProperty("username") + " in username");
		loginPageObj.getPassword().sendKeys(prop.getProperty("password"));
		log.debug("Password inputted");
		extentTest.get().log(Status.INFO, "User input " + prop.getProperty("password") + " in password");
		loginPageObj.getLoginButton().click();
		log.debug("Login button clicked");
		extentTest.get().log(Status.INFO, "User clicked on login button to sign-in");
		HomePage homePage = new HomePage(driver);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions
				.elementToBeClickable(homePage.getAddPortfolioButton()));
		log.debug("User successfully logged in");
		extentTest.get().log(Status.INFO, "User successfully logged in");
		homePage.getProfileIcon().click();
		log.debug("User clicked on profile icon");
		extentTest.get().log(Status.INFO, "User clicked on profile icon");
		homePage.getLogoutLink().click();
		log.debug("User clicked on log out");
		extentTest.get().log(Status.INFO, "User clicked on log out");
		WebDriverWait wait3 = new WebDriverWait(driver, 7);
		wait3.until(ExpectedConditions.textToBePresentInElement(defaultPageObj.getLoginButtonWebelement(), "Log In"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("history.go(-1);", new Object[0]);
		log.debug("Browser back button clicked");
		extentTest.get().log(Status.INFO, "User clicked on browser back button");
		Assert.assertEquals(driver.getCurrentUrl(), "https://www.sigfig.com/site/#/login?next=%2Fholdings");
		log.debug("User remains logged out");
		extentTest.get().log(Status.INFO, "User remains logged out");
	}

	@AfterTest
	public void teardown() {
		log.debug("Closing browser");
		driver.close();
	}

}
