package testcases;

import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.aventstack.extentreports.Status;
import pages.DefaultPage;
import pages.LoginPage;
import resources.Base;

public class CredentialsInUpperCase extends Base {
	public WebDriver driver;
	static final Logger log = LogManager.getLogger(CredentialsInUpperCase.class.getName());

	@BeforeTest
	public void initialize() throws IOException {
		log.debug("Opening chrome browser");
		driver = initializeDriver();
	}

	@Test
	public void credentialsInUpperCase() throws InterruptedException {
		driver.get(prop.getProperty("url"));
		log.debug("SigFig website launched");
		extentTest.get().log(Status.INFO, "SigFig site launched");
		DefaultPage defaultPageObj = new DefaultPage(driver);
		defaultPageObj.getLoginButtonWebelement().click();
		log.debug("Login button of default page clicked");
		extentTest.get().log(Status.INFO, "User clicked on login button of default page");
		LoginPage loginPageObj = new LoginPage(driver);
		loginPageObj.getUsername().sendKeys(prop.getProperty("username").toUpperCase());
		log.debug("Username inputted");
		extentTest.get().log(Status.INFO, "User input " + prop.getProperty("username").toUpperCase() + " in username");
		loginPageObj.getPassword().sendKeys(prop.getProperty("password").toUpperCase());
		log.debug("Password inputted");
		extentTest.get().log(Status.INFO, "User input " + prop.getProperty("password").toUpperCase() + " in password");
		loginPageObj.getLoginButton().click();
		log.debug("Login button clicked");
		extentTest.get().log(Status.INFO, "User clicked on login button to sign-in");
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOf(loginPageObj.getInvalidPasswordMessage()));
		String expectedMessage = "The username and password you entered don't match our records. Please check them and try again.";
		String actualMessage = loginPageObj.getInvalidPasswordMessage().getText();
		String expectedClass = "text-danger form-error sf-animate slide-and-fade-bonus";
		String actualClass = loginPageObj.getInvalidPasswordMessage().getAttribute("class");
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(actualMessage, expectedMessage);
		softAssert.assertEquals(actualClass, expectedClass);
		softAssert.assertAll();
		String wrongPasswordMessageReport = REPORTACTUALSTRING + actualMessage + REPORTEXPECTEDSTRING + expectedMessage
				+ "]";
		extentTest.get().log(Status.INFO, wrongPasswordMessageReport);
		String wrongPasswordMessageReportClass = REPORTACTUALSTRING + actualClass + REPORTEXPECTEDSTRING + expectedClass
				+ "]";
		extentTest.get().log(Status.INFO, wrongPasswordMessageReportClass);
		log.debug("Validation success");
	}

	@AfterTest
	public void teardown() {
		log.debug("Closing browser");
		driver.close();
	}
}
